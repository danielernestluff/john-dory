//
//  DELtableViewCell.h
//  walkinn-tt
//
//  Created by Daniel-Ernest Luff on 04/08/2015.
//  Copyright (c) 2015 danielernestluff. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DELtableViewCell : UITableViewCell 

@property (nonatomic, strong) IBOutlet UILabel *mainText;
@property (nonatomic, strong) IBOutlet UILabel *catagory;
@property (nonatomic, strong) IBOutlet UILabel *rating;
@property (nonatomic, strong) IBOutlet UIImageView *photos;
@property (nonatomic, strong) IBOutlet UILabel *discountLabel;
@property (nonatomic, strong) IBOutlet UILabel *tablesLabel;

@end