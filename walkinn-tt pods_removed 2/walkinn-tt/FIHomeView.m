//
//  FIHomeView.m
//  findItt
//
//  Created by Daniel-Ernest Luff on 27/09/2015.
//  Copyright © 2015 toutright. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FIHomeView.h"
#import "DELPopupView.h"
#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@implementation FIHomeView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _mapView.delegate = self;
    locationManager = [[CLLocationManager alloc] init];;
    locationManager.delegate = self;
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        // Use one or the other, not both. Depending on what you put in info.plist
        [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    [locationManager startUpdatingLocation];
    
    _mapView.showsUserLocation = YES;
    [_mapView setCenterCoordinate:_mapView.userLocation.location.coordinate animated:YES];
    _mapView.userTrackingMode = MKUserTrackingModeFollow;
    self.locationController = [[FILocationController alloc] init];
    self.locationController.delegate = self;
    [self.locationController.locationManager startUpdatingLocation];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
    [self searchsetup];
    });

}

typedef void(^addressCompletion)(NSString *);

-(void)getAddressFromLocation:(CLLocation *)location complationBlock:(addressCompletion)completionBlock
{
    __block CLPlacemark* placemark;
    __block NSString *address = nil;
    
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             placemark = [placemarks lastObject];
             address = [NSString stringWithFormat:@"%@", placemark.subLocality];
             completionBlock(address);
         }
     }];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        // Send the user to the Settings for this app
        NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:settingsURL];
    }
}
#pragma mark - Search Bar

- (void)searchsetup {
    
    CLLocation* eventLocation = [[CLLocation alloc] initWithLatitude:_mapView.userLocation.location.coordinate.latitude longitude:_mapView.userLocation.location.coordinate.longitude];
    
    [self getAddressFromLocation:eventLocation complationBlock:^(NSString * address) {
        if(address) {
            NSLog(@"%@", address);
            _searchBar.text = address;
        }
    }];
    
}

#pragma mark - Options Popup View

-(void) showInviteCard {
    if(!self.popupView)
        self.popupView = [DELPopupView popupView];
    [self.popupView.handleView addSubview:self.popUpViewController];
    self.popUpViewController.center = CGPointMake(self.popupView.handleView.frame.size.width/2.0,
                                        self.popupView.handleView.frame.size.height/2.0);
    [self.popupView show];
}


- (IBAction)showPressed:(id)sender{
    [self showInviteCard];
    
   
    
//    NSError *error;
//    NSDictionary *requestReply = [NSJSONSerialization JSONObjectWithData:locationManager.location.coordinate options:NSJSONReadingAllowFragments error:&error]
//    if (requestReply) {
//        //use the dictionary
//    }
//    else {
//        NSLog("Error parsing JSON: %@", error);
//    }
    
    NSError *error;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:_latt forKey:@"latitude"];
    [dict setValue:_longg forKey:@"longitude"];
    
    
    //convert object to data
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    NSLog(@"%@", jsonData.description);
    
    
    [PFCloud callFunctionInBackground:@"callFourSquare"
                       withParameters:@{@"geo": jsonData}
                                block:^(NSArray *results, NSError *error) {
                                    if (!error) {
                                        NSLog(@"%@", results);
                                        // this is where you handle the results and change the UI.
                                        
                                        
                                    } else {
                                        NSLog(@"%@", error);
                                    }
                                }];

}

//- (IBAction)sendEmail:(id)sender{
//    [[Mixpanel sharedInstance] track:kMPEvent_emailInvite];
//    NSString * subject = @"Join Hitch now";
//    //email body
//    HTProfile *userProfile = [HTAppDelegate sharedInstance].profile;
//    NSString * body = [NSString stringWithFormat:@"Use my invite code %@ to join Hitch. Invite expires in 24hrs. http://www.hitchapp.co/download", userProfile.invite_code];
//    
//    //create the MFMailComposeViewController
//    MFMailComposeViewController * composer = [[MFMailComposeViewController alloc] init];
//    composer.mailComposeDelegate = self;
//    [composer setSubject:subject];
//    [composer setMessageBody:body isHTML:NO];
//    //[composer setMessageBody:body isHTML:YES]; //if you want to send an HTML message
//    
//    //get the filepath from resources
//    // NSString *filePath = [[NSBundle mainBundle] pathForResource:@"" ofType:@""];
//    
//    //read the file using NSData
//    //  NSData * fileData = [NSData dataWithContentsOfFile:filePath];
//    // Set the MIME type
//    /*you can use :
//     - @"application/msword" for MS Word
//     - @"application/vnd.ms-powerpoint" for PowerPoint
//     - @"text/html" for HTML file
//     - @"application/pdf" for PDF document
//     - @"image/jpeg" for JPEG/JPG images
//     */
//    // NSString *mimeType = @"image/png";
//    
//    //add attachement
//    // [composer addAttachmentData:fileData mimeType:mimeType fileName:filePath];
//    
//    //present it on the screen
//    [self presentViewController:composer animated:YES completion:NULL];
//}
//
//- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
//    switch (result) {
//        case MFMailComposeResultCancelled:
//            NSLog(@"Mail cancelled"); break;
//        case MFMailComposeResultSaved:
//            NSLog(@"Mail saved"); break;
//        case MFMailComposeResultSent:
//            [[Mixpanel sharedInstance] track:kMPEvent_emailInviteSuc];
//            NSLog(@"Mail sent"); break;
//        case MFMailComposeResultFailed:
//            NSLog(@"Mail sent failure: %@", [error localizedDescription]); break;
//        default:
//            break;
//    }
//    
//    // close the Mail Interface
//    [self dismissViewControllerAnimated:YES completion:NULL];
//}
//
//
//- (IBAction)sendSMS:(id)sender{
//    //check if the device can send text messages
//    if(![MFMessageComposeViewController canSendText]) {
//        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device cannot send text messages" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//        return;
//    }
//    [[Mixpanel sharedInstance] track:kMPEvent_smsInvite];
//    
//    //set receipients
//    NSArray *recipients = [NSArray arrayWithObjects: nil];
//    
//    //set message text
//    HTProfile *userProfile = [HTAppDelegate sharedInstance].profile;
//    NSString * message = [NSString stringWithFormat:@"Use my invite code %@ to join Hitch. Invite expires in 24hrs. http://www.hitchapp.co/download", userProfile.invite_code];
//    
//    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
//    messageController.messageComposeDelegate = self;
//    [messageController setRecipients:recipients];
//    [messageController setBody:message];
//    
//    // Present message view controller on screen
//    [self presentViewController:messageController animated:YES completion:nil];
//}
//
//- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
//{
//    switch (result) {
//        case MessageComposeResultCancelled:
//            break;
//        case MessageComposeResultFailed:{
//            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [warningAlert show];
//            break;
//        }
//        case MessageComposeResultSent:
//            [[Mixpanel sharedInstance] track:kMPEvent_smsInviteSuc];
//            break;
//        default:
//            break;
//    }
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
//
- (IBAction)sendWhatsapp:(id)sender{
    NSString * msg = [NSString stringWithFormat:@"Hey! I'm using findItt to find the best venues nearby, you should download it here:" ];
    
    msg = [msg stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    msg = [msg stringByReplacingOccurrencesOfString:@":" withString:@"%3A"];
    msg = [msg stringByReplacingOccurrencesOfString:@"/" withString:@"%2F"];
    msg = [msg stringByReplacingOccurrencesOfString:@"?" withString:@"%3F"];
    msg = [msg stringByReplacingOccurrencesOfString:@"," withString:@"%2C"];
    msg = [msg stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
    msg = [msg stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    
    NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?text=%@",msg];
    NSURL * whatsappURL = [NSURL URLWithString:urlWhats];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
    {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    } else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed" message:@"Your device does not have WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}
//- (IBAction)sendFacebook:(id)sender{
//    //UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@"Not implemented"
//    //                                                    message:@"Will do so soon."
//    //
//    //                                                   delegate:self
//    //                                          cancelButtonTitle:@"OK"
//    //                                          otherButtonTitles:nil];
//    // [theAlert show];
//    [[Mixpanel sharedInstance] track:kMPEvent_facebookInvite];
//    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
//    content.contentURL = [NSURL URLWithString:@"http://www.hitchapp.co/download"];
//    [FBSDKShareDialog showFromViewController:self
//                                 withContent:content
//                                    delegate:nil];
//    
//    // FBSDKSendButton *button = [[FBSDKSendButton alloc] init];
//    // button.shareContent = content;
//    // [self.view addSubview:button];
//}

#pragma mark - Location Debugging

- (void)update:(CLLocation *)location {
    NSLog(@"Latitude: %f", [location coordinate].latitude);
    NSLog(@"Longitude: %f", [location coordinate].longitude);
    
    NSString *sLat = [NSString stringWithFormat:@"%f", [location coordinate].latitude];
    NSString *sLong = [NSString stringWithFormat:@"%f", [location coordinate].longitude];
    
    _latt = sLat;
    _longg = sLong;

}

- (void)locationError:(NSError *)error {
    NSLog(@"error %@", [error description]);
}


#pragma mark - In App Purchace 

//- (IBAction)restore:(id)sender
//{
//    // Call StoreObserver to restore all restorable purchases
//    [[StoreObserver sharedInstance] restore];
//}

@end