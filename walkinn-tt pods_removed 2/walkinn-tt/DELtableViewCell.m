//
//  DELtableViewCell.m
//  
//
//  Created by Daniel-Ernest Luff on 04/08/2015.
//
//

#import <Foundation/Foundation.h>
#import "DELtableViewCell.h"

@implementation DELtableViewCell

@synthesize mainText = _mainText;
@synthesize catagory = _catagory;
@synthesize rating = _rating;
@synthesize photos = _photos;
@synthesize discountLabel = _discountLabel;
@synthesize tablesLabel = _tablesLabel;


@end

