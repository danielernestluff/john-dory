//
//  FILocationController.m
//  findItt
//
//  Created by Daniel-Ernest Luff on 11/10/2015.
//  Copyright © 2015 toutright. All rights reserved.
//

#import "FILocationController.h"
#import "CoreLocation/CoreLocation.h"

@implementation FILocationController

- (id)init {
    self = [super init];
    if(self != nil) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
    }
    return self;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    [self.delegate update:newLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [self.delegate locationError:error];
}

@end