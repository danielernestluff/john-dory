//
//  AppDelegate.h
//  walkinn-tt
//
//  Created by Daniel-Ernest Luff on 03/08/2015.
//  Copyright (c) 2015 danielernestluff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "FIHomeView.h"
#import <Parse/Parse.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) FIHomeView *FIHV;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

