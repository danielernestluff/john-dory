//
//  DELCellView.h
//  walkinn-tt
//
//  Created by Daniel-Ernest Luff on 04/08/2015.
//  Copyright (c) 2015 danielernestluff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DELtableView.h"
#import "FourSquareKit.h"
#import "MZTimerLabel.h"
#import "STKSpinnerView.h"

@interface DELCellView : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout> {UILabel *progress;
    NSTimer *timer;
    int currMinute;
    int currSeconds;
    MZTimerLabel *timerExample5;}

@property (weak, nonatomic) IBOutlet STKSpinnerView *spinnerView;
@property (nonatomic, strong) DELtableView *receipt;
@property (nonatomic, strong) IBOutlet UIImageView *qrcode;
@property(nonatomic,strong) UXRFourSquareNetworkingEngine *fourSquareEngine;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic) NSInteger counter;
@property (nonatomic, strong) NSMutableArray *congratsmessages;
@property (weak, nonatomic) IBOutlet UILabel *lblTimerExample5;
@property (weak, nonatomic) IBOutlet UILabel *congrats;
@property (weak, nonatomic) IBOutlet UILabel *showtoserver;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (nonatomic, strong) NSMutableArray *success;
@property (nonatomic, strong) IBOutlet UIProgressView *progressView;
@property (nonatomic, strong) NSTimer *myTimer;
@property (nonatomic) int *myInt;
@property (nonatomic, strong) IBOutlet UIButton *qrbutton;
@property (nonatomic, strong) IBOutlet UIButton *addressbutton;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *tables;
@property (weak, nonatomic) IBOutlet UILabel *rating;


@end