//
//  DELCOLCELL.h
//  walkinn-tt
//
//  Created by Daniel-Ernest Luff on 05/08/2015.
//  Copyright (c) 2015 danielernestluff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFDropdownNotification.h"
#import <CoreLocation/CoreLocation.h>

@interface DELcolCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UIImageView *restPictures;

@end
