//
//  DELtableView.h
//  walkinn-tt
//
//  Created by Daniel-Ernest Luff on 03/08/2015.
//  Copyright (c) 2015 danielernestluff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFDropdownNotification.h"
#import <CoreLocation/CoreLocation.h>


@interface DELtableView : UIViewController <UITableViewDataSource, UITableViewDelegate, AFDropdownNotificationDelegate, CLLocationManagerDelegate> {IBOutlet UITableView *mainTableView;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;}



@end