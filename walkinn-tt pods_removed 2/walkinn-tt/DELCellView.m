//
//  DELCellView.m
//  walkinn-tt
//
//  Created by Daniel-Ernest Luff on 04/08/2015.
//  Copyright (c) 2015 danielernestluff. All rights reserved.
//

#define progressPlus 1.00
#define progressMinus -=0.01

#import "DELCellView.h"
#import "FourSquareKit.h"
#import <CoreImage/CoreImage.h>
#import "FourSquareKit.h"
#import "DELCOLCELL.h"
#import "MZTimerLabel.h"

@implementation DELCellView

-(void) viewDidLoad
{
    [super viewDidLoad];
    
    NSString *yourClientId = @"ZQEJ0SK5ONCJ35OYUMRQXOQCOFBIAABWVXOQRDT45YPZAFEB";
    NSString *yourClientSecret = @"134STZL4QZCOAF5KSNXP21A2S2G1JSLFVSPXUIX3LEOPRYS4";
    NSString *yourCallbackURl = @"//yourapp://foursquare";
    [UXRFourSquareNetworkingEngine registerFourSquareEngineWithClientId:yourClientId andSecret:yourClientSecret andCallBackURL:yourCallbackURl];
    self.fourSquareEngine = [UXRFourSquareNetworkingEngine sharedInstance];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [self.collectionView registerNib:[UINib nibWithNibName:@"DELCOLCELL" bundle:nil] forCellWithReuseIdentifier:@"colcell"];
    [self.collectionView reloadData];
    [self.collectionView layoutIfNeeded];
    //[self.collectionView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    //[self.collectionView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 200)];
    [self.collectionView setContentOffset:CGPointMake(0, 0)];
    [self.collectionView setScrollEnabled:YES];
    [self.collectionView setShowsHorizontalScrollIndicator:NO];
    [self.collectionView setShowsVerticalScrollIndicator:NO];
    [self.collectionView setPagingEnabled:YES];
    [self startCountdown];
    self.progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    [self.progressView setTransform:CGAffineTransformMakeScale(1.0, 3.0)];
    [self.progressView setFrame:CGRectMake(0, 0, 300, 25)];
    _success= [[NSMutableArray alloc] initWithObjects:
              @"Nice one!",
              @"Good catch!",
              @"Feeling hungry?",
              @"Enjoy!",
              @"T-minus 30 minutes",
              nil];
    
    self.congrats.text = [[NSString alloc] initWithFormat:
                               @"%@", [_success objectAtIndex:arc4random()%[_success count]]];
    
    UXRFourSquareRestaurantModel *restaurantModel = self.receipt;
    self.address.text = [NSString stringWithFormat:@"%@\n%@\n%@", restaurantModel.location.address, restaurantModel.location.city, restaurantModel.location.postalCode];
    self.name.text = restaurantModel.name;
    float value = [restaurantModel.rating floatValue];
    float rounded = ((int)(value * 1 + .5) / 1);
    NSLog(@"Rating %f", rounded);
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc]init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setUsesGroupingSeparator:NO];
    [formatter setMaximumFractionDigits:0];
    [formatter setMinimumFractionDigits:0];
    NSString *thevalue = [formatter stringFromNumber:restaurantModel.rating];
    //NSLog(@"number = %@, value);
    self.rating.text = [NSString stringWithFormat:@"%@/10", thevalue];;
    //NSLog(@"Description %s", restaurantModel.description.UTF8String);
    [self setupCollectionView];
    
}

- (void)startCountdown
{
    timerExample5 = [[MZTimerLabel alloc] initWithLabel:_lblTimerExample5 andTimerType:MZTimerLabelTypeTimer];
    [timerExample5 setCountDownTime:1800];
}




//-(IBAction)QRCode:(id)sender {
//   
//    UXRFourSquareRestaurantModel *restaurantModel = self.receipt;
//    NSString *urlStr =[NSString stringWithFormat:@"%@", restaurantModel.url];
//    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
//    
//    //    NSLog(@"filterAttributes:%@", filter.attributes);
//    
//    [filter setDefaults];
//    
//    NSData *data = [urlStr dataUsingEncoding:NSUTF8StringEncoding];
//    [filter setValue:data forKey:@"inputMessage"];
//    
//    CIImage *outputImage = [filter outputImage];
//    
//    CIContext *context = [CIContext contextWithOptions:nil];
//    CGImageRef cgImage = [context createCGImage:outputImage
//                                       fromRect:[outputImage extent]];
//    
//    UIImage *image = [UIImage imageWithCGImage:cgImage
//                                         scale:1.
//                                   orientation:UIImageOrientationUp];
//    
//    // Resize without interpolating
//    //UIImage *resized = [self resizeImage:image
//                             withQuality:kCGInterpolationNone
//                                    rate:5.0];
//    self.qrcode.image = resized;
//    [timerExample5 start];
//    CGImageRelease(cgImage);
//    [self setupProgress];
//    [UIView animateWithDuration:0.5 animations:^{_lblTimerExample5.alpha = 1.0;}];
//    [UIView animateWithDuration:0.5 animations:^{_congrats.alpha = 1.0;}];
//    [UIView animateWithDuration:0.5 animations:^{_showtoserver.alpha = 1.0;}];
//    [UIView animateWithDuration:0.5 animations:^{_qrcode.alpha = 1.0;}];
//    [UIView animateWithDuration:0.5 animations:^{_address.alpha = 0.0;}];
//    [UIView animateWithDuration:0.5 animations:^{_progressView.alpha = 0.0;}];
//    [UIView animateWithDuration:0.5 animations:^{_qrbutton.alpha = 0.0;}];
//    [UIView animateWithDuration:0.5 animations:^{_addressbutton.alpha = 1.0;}];
//    [UIView animateWithDuration:0.5 animations:^{_spinnerView.alpha = 1.0;}];
//    
//    //[self.collectionView reloadData];
//    [[self spinnerView] setImage:[UIImage imageNamed:nil]];
//    
//    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(spinit:) userInfo:nil repeats:YES];
//}

- (void)spinit:(NSTimer *)timer
{
    static float prog = 0.0;
    prog += 0.00005555555556;
    if(prog >= 1.0) {
        prog = 1.0;
        [timer invalidate];
    }
    [[self spinnerView] setProgress:prog animated:YES];
}


-(IBAction)showAddress:(id)sender {
    [UIView animateWithDuration:0.5 animations:^{_lblTimerExample5.alpha = 0.0;}];
    [UIView animateWithDuration:0.5 animations:^{_congrats.alpha = 0.0;}];
    [UIView animateWithDuration:0.5 animations:^{_showtoserver.alpha = 0.0;}];
    [UIView animateWithDuration:0.5 animations:^{_qrcode.alpha = 0.0;}];
    [UIView animateWithDuration:0.5 animations:^{_address.alpha = 1.0;}];
    [UIView animateWithDuration:0.5 animations:^{_progressView.alpha = 1.0;}];
    [UIView animateWithDuration:0.5 animations:^{_qrbutton.alpha = 1.0;}];
    [UIView animateWithDuration:0.5 animations:^{_addressbutton.alpha = 0.0;}];
    [UIView animateWithDuration:0.5 animations:^{_spinnerView.alpha = 0.0;}];
}

- (void) setupProgress {
    // Reset everything and start moving the progressbar near its end of doom!
    [self.progressView setProgress:progressPlus];
    timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(updateProgress)
                                           userInfo:nil
                                            repeats:YES];
}

- (void) updateProgress {
    
   // NSLog(@"%f",progressMinus);
    
    //[self.progressView setProgress:progressMinus];
    if(progressPlus <= 0.0f) {
        [timer invalidate]; // stops timer, removes the method from the loop.
        
        // Alert setup
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Time is up!"
                                                        message:[NSString stringWithFormat:@"Done!"]
                                                       delegate:self
                                              cancelButtonTitle:@"Play Again"
                                              otherButtonTitles:nil];
        // Show the alert
        [alert show];
    }
}
-(void)setupCollectionView {
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [self.collectionView setPagingEnabled:YES];
    [self.collectionView setCollectionViewLayout:flowLayout];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 4;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    DELcolCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"colcell" forIndexPath:indexPath];
    UXRFourSquareRestaurantModel *restaurantModel = self.receipt;
    [self.fourSquareEngine getPhotosForRestaurantWithId:restaurantModel.restaurantId
                                    withCompletionBlock:^(NSArray *photos) {
                                        UXRFourSquarePhotoModel *photoModel = (UXRFourSquarePhotoModel *)photos[indexPath.item];
                                        // Download the image to your image view
                                        NSLog(@"YES");
                                        NSURL *fullPhotoURL = [photoModel fullPhotoURL];
                                        NSData *data = [NSData dataWithContentsOfURL:fullPhotoURL];
                                        UIImage *image = [UIImage imageWithData:data];
                                        cell.restPictures.image = image;
                                        NSLog(@"Address & DEscription %@ - %lu",restaurantModel.location, (unsigned long)restaurantModel.categories[0]);
                                        // Use this URL to fetch the photo however you like to do that...
                                    } failureBlock:^(NSError *error) {
                                        // Error.
                                        NSLog(@"NO");
                                    }];
    

    
    return cell;
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return self.collectionView.frame.size;
}

@end