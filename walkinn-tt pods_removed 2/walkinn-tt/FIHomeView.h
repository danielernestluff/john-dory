//
//  FIHomeView.h
//  findItt
//
//  Created by Daniel-Ernest Luff on 27/09/2015.
//  Copyright © 2015 toutright. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/Mapkit.h>
#import "DELPopupView.h"
#import "FILocationController.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface FIHomeView : UIViewController <CLLocationManagerDelegate, FICoreLocationControllerDelegate, MKMapViewDelegate> {CLLocationManager *locationManager;
    CLLocation *currentLocation;}

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet DELPopupView *popupView;
@property (strong, nonatomic) IBOutlet UIView *popUpViewController;
@property (nonatomic, retain) NSString *latt;
@property (nonatomic, retain) NSString *longg;
@property (nonatomic, retain) FILocationController *locationController;
@property (weak) IBOutlet UITextField *searchBar;


@end