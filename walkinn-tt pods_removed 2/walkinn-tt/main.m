//
//  main.m
//  walkinn-tt
//
//  Created by Daniel-Ernest Luff on 03/08/2015.
//  Copyright (c) 2015 danielernestluff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
