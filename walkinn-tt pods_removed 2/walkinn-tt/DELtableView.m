//
//  DELtableView.m
//  walkinn-tt
//
//  Created by Daniel-Ernest Luff on 03/08/2015.
//  Copyright (c) 2015 danielernestluff. All rights reserved.
//

#import "DELtableView.h"
#import "DELtableViewCell.h"
#import "DELCellView.h"
#import "FourSquareKit.h"

@interface DELtableView ()

@property (nonatomic, strong) AFDropdownNotification *notification;
@property (nonatomic, strong) NSArray *successMessages;
@property(nonatomic,strong) UXRFourSquareNetworkingEngine *fourSquareEngine;
@property(nonatomic,strong) NSMutableArray *discount;
@property(nonatomic,strong) NSMutableArray *tables;
@property (strong,nonatomic) NSArray *colors;

@end

@implementation DELtableView

-(void)viewDidAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *yourClientId = @"ZQEJ0SK5ONCJ35OYUMRQXOQCOFBIAABWVXOQRDT45YPZAFEB";
    NSString *yourClientSecret = @"134STZL4QZCOAF5KSNXP21A2S2G1JSLFVSPXUIX3LEOPRYS4";
    NSString *yourCallbackURl = @"//yourapp://foursquare";
    
    [UXRFourSquareNetworkingEngine registerFourSquareEngineWithClientId:yourClientId andSecret:yourClientSecret andCallBackURL:yourCallbackURl];
    self.fourSquareEngine = [UXRFourSquareNetworkingEngine sharedInstance];
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage new]
    //                                              forBarMetrics:UIBarMetricsDefault];
    //self.navigationController.navigationBar.shadowImage = [UIImage new];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];

    NSLog(@"called!");
    NSString *query = @"restaurants";
    CLLocationCoordinate2D coordinate = [CLLocation locationInLondon].coordinate;
    [self.fourSquareEngine exploreRestaurantsNearLatLong:coordinate withQuery:query
                                     withCompletionBlock:^(NSArray *restaurants) {

                                         _successMessages = restaurants;
                                         //NSLog(@"Name: %@", restaurantModel.name);
                                         NSLog(@"qui %@", restaurants);
                                         [mainTableView reloadData];
                                     } failureBlock:^(NSError *error) {
                                         NSLog(@"non");
                                     }];

    

    //mainTableView.separatorStyle = UITableViewStylePlain;
    //mainTableView.opaque=NO;
    //mainTableView.showsHorizontalScrollIndicator=NO;
    //mainTableView.showsVerticalScrollIndicator=NO;
        // Do any additional setup after loading the view, typically from a nib.
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    UIButton *rightbutton = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [rightbutton addTarget:self action:@selector(demoAlertView:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightbutton];
    _notification = [[AFDropdownNotification alloc] init];
    _notification.notificationDelegate = self;
    [self showAlertForFirstRun];
    
    [self currentLocationIdentifier];
    
    [mainTableView registerNib:[UINib nibWithNibName:@"DELtableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [mainTableView reloadData];
    
    _discount= [[NSMutableArray alloc] initWithObjects:
                       @"20",
                       @"25",
                       @"30",
                       @"35",
                       @"40",
                       @"45",
                        @"50",nil];
    
    _tables= [[NSMutableArray alloc] initWithObjects:
                @"2 3 4",nil];
    
    
}

-(void)currentLocationIdentifier
{
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (!(error))
         {
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSLog(@"\nCurrent Location Detected\n");
             NSLog(@"placemark %@",placemark);
             NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
             NSString *Address = [[NSString alloc]initWithString:locatedAt];
             NSString *Area = [[NSString alloc]initWithString:placemark.locality];
             NSString *Country = [[NSString alloc]initWithString:placemark.country];
             NSString *CountryArea = [NSString stringWithFormat:@"%@, %@", Area,Country];
             NSLog(@"%@",CountryArea);
         }
         else
         {
             NSLog(@"Geocode failed with error %@", error);
             NSLog(@"\nCurrent Location Not Detected\n");
             //return;
             //CountryArea = NULL;
         }
         /*---- For more results
          placemark.region);
          placemark.country);
          placemark.locality);
          placemark.name);
          placemark.ocean);
          placemark.postalCode);
          placemark.subLocality);
          placemark.location);
          ------*/
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[self foursquareCall];
    static NSString *simpleTableIdentifier = @"cell";
    
    DELtableViewCell *cell = (DELtableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"cell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    UXRFourSquareRestaurantModel *restaurantModel = _successMessages[indexPath.row];
    NSLog(@"name %@", restaurantModel.name);
    cell.mainText.text = restaurantModel.name;
//    if (restaurantModel.categories == 0) {
//        cell.catagory.text = @"resturant";
//    } else {
//        cell.catagory.text = restaurantModel.categories;
//    }
    
    cell.rating.text = [NSString stringWithFormat:@"%@", restaurantModel.rating];
    cell.discountLabel.text = [[NSString alloc] initWithFormat:
                                     @"%@%%", [_discount objectAtIndex:arc4random()%[_discount count]]];
    cell.tablesLabel.text = [[NSString alloc] initWithFormat:
                               @"Tables for: %@", [_tables objectAtIndex:arc4random()%[_tables count]]];
    [self.fourSquareEngine getPhotosForRestaurantWithId:restaurantModel.restaurantId
                                    withCompletionBlock:^(NSArray *photos) {
                                        UXRFourSquarePhotoModel *photoModel = (UXRFourSquarePhotoModel *)photos[0];
                                        // Download the image to your image view
                                        NSURL *fullPhotoURL = [photoModel fullPhotoURL];
                                        NSData *data = [NSData dataWithContentsOfURL:fullPhotoURL];
                                        UIImage *image = [UIImage imageWithData:data];
                                        cell.photos.image = image;
                                        // Use this URL to fetch the photo however you like to do that...
                                    } failureBlock:^(NSError *error) {
                                        // Error.
                                    }];
    
    float value = [restaurantModel.rating floatValue];
    float rounded = ((int)(value * 1 + .5) / 1);
    NSLog(@"Rating %f", rounded);
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc]init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setUsesGroupingSeparator:NO];
    [formatter setMaximumFractionDigits:0];
    [formatter setMinimumFractionDigits:0];
    NSString *thevalue = [formatter stringFromNumber:restaurantModel.rating];
    //NSLog(@"number = %@, value);
    cell.rating.text = [NSString stringWithFormat:@"%@/10", thevalue];;
    
    
    
    //NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[restaurantModel.photos objectAtIndex:indexPath.row]]];
    //cell.photos.image = [UIImage imageWithData:imageData];
    //[cell.photos setImageWithURL:[restaurantModel.photos objectAtIndex:indexPath.row] placeholderImage:[[UIImage alloc] init] options:nil];
//    NSString *urlStr =[NSString stringWithFormat:@"%@", [restaurantModel.photos objectAtIndex:indexPath.row]];
//    NSURL *url = [NSURL URLWithString:urlStr];
//    NSData *data = [NSData dataWithContentsOfURL:url];
//    UIImage *image = [UIImage imageWithData:data];
//    cell.photos.image = image;
    //NSLog(@"Photos %@", [NSURL URLWithString:[restaurantModel.photos objectAtIndex:indexPath.row]]);
    //cell.catagory.text = restaurantModel.categories;
    //cell.thumbnailImageView.image = [UIImage imageNamed:[thumbnails objectAtIndex:indexPath.row]];
    //cell.prepTimeLabel.text = [prepTime objectAtIndex:indexPath.row];
    return cell;
    
    //DELtableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    //NSString *yourString = @"Your invite code is %ld",(long)indexPath.row;
    //;;
    //return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DELtableView *receipt = [_successMessages objectAtIndex:indexPath.row];
    
    DELCellView *controller = [[DELCellView alloc] initWithNibName:@"DELCellView" bundle:nil];
    
    // Pass data to controller
    controller.receipt = receipt;
    [self.navigationController pushViewController:controller animated:YES];
    NSLog(@"TableCellPressed");
    //DELCellView *vc2 = [[DELCellView alloc] init];
    //[self.navigationController pushViewController:vc2 animated:YES];

}

- (void)showAlertForFirstRun
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    static NSString *DidFirstRunKey = @"DidFirstRun";
    BOOL didFirstRun = [userDefaults boolForKey:DidFirstRunKey];
    if (didFirstRun)
    {
        _notification.titleText = @"Update available";
        _notification.subtitleText = @"Do you want to download the update of this file?";
        _notification.image = [UIImage imageNamed:@"update"];
        _notification.topButtonText = @"Accept";
        _notification.bottomButtonText = @"NON";
        _notification.dismissOnTap = YES;
        [_notification presentInView:self.view withGravityAnimation:YES];
        
        [_notification listenEventsWithBlock:^(AFDropdownNotificationEvent event) {
            
            switch (event) {
                case AFDropdownNotificationEventTopButton:
                    // Top button
                    break;
                    
                case AFDropdownNotificationEventBottomButton:
                    //bottom
                    break;
                    
                case AFDropdownNotificationEventTap:
                    // Tap
                    break;
                    
                default:
                    break;
            }
        }];
        
        NSLog(@"show notification");
    }
}


-(IBAction)demoAlertView:(id)sender {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Not Implemented" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    return;
}


@end
