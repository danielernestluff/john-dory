Parse.Cloud.define("callFourSquare", function(request, response) {
//The Parse GeoPoint with current location for search
    var geo = request.params.location;
    var geoJson = geo.toJSON();
    var url = "https://api.foursquare.com/v2/venues/explore?ll=" + geoJson.latitude + ","
        + geoJson.longitude +         "&section=food&sortByDistance=1&limit=50&venuePhotos=1&categoryId=4d4b7105d754a06374d81259&client_id=ZQEJ0SK5ONCJ35OYUMRQXOQCOFBIAABWVXOQRDT45YPZAFEB"
        + "&client_secret=134STZL4QZCOAF5KSNXP21A2S2G1JSLFVSPXUIX3LEOPRYS4&v=20140715";
        console.log(url);
    //Call to FourSquare api, which returns list of restaurants and their details
    Parse.Cloud.httpRequest({
        method: "GET",
        url: url,
        success: function (httpResponse) {
            var restaurants = [];
            var json = httpResponse.data;
            var venues = json.response.groups[0].items;
            console.log(venues.length)
            for(i = 0; i < venues.length; i++) {
                venue = venues[i].venue;

                var RestaurantObject =  Parse.Object.extend("Restaurant");
                var rest = new RestaurantObject();
                try {
                    rest.set("geoLocation",
                    new Parse.GeoPoint({latitude: venue.location.lat,
                        longitude: venue.location.lng}));

                } catch(err) {}
                try {
                    rest.set("address", venue.location.address + " " +     venue.location.formattedAddress[1]);
                } catch(err) {}
                try {
                    rest.set("phoneNumber", venue.contact.formattedPhone);
                } catch(err) {}
                try {
                    rest.set("website", venue.url);
                } catch(err) {}
                rest.set("name", venue.name);
                rest.set("lowerName", venue.name.toLowerCase());
                try {
                    rest.set("priceLevel", venue.price.tier);
                } catch(err) {}
                try {
                    rest.set("rating", venue.rating/2);
                } catch(err) {}
                try {
                    rest.set("storeId", venue.id);
                } catch(err) {}
                try {
                    rest.set("icon", venue.photos.groups[0].items[0].prefix + "original"
                        + venue.photos.groups[0].items[0].suffix)
                } catch(err) {}

                restaurants.push(rest);

            }
            Parse.Object.saveAll();
        },
        error: function (httpResponse) {
            response.error("Request failed with response code:" + httpResponse.status + "     Message: "
                + httpResponse.text);
        }
    });
});
